//
// Created by Artemij on 25.12.2021.
//

#include "image.h"
#include "mm_malloc.h"

struct image some_image(const uint64_t width, const uint64_t height, struct pixel *data) {
    return (struct image) {.width = width, .height = height, .data = data};
}

struct image image_create(const uint64_t width, const uint64_t height) {
    struct pixel *pixels = malloc(sizeof(struct pixel) * width * height);
    return some_image(width, height, pixels);
}

void image_destroy(const struct image *image) {
    free(image->data);
}
