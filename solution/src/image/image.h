//
// Created by Artemij on 25.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
#define ASSIGNMENT_IMAGE_ROTATION_IMAGE_H

#include <stdbool.h>
#include <stdint.h>

struct pixel {
    uint8_t r;
    uint8_t g;
    uint8_t b;
};

struct image {
    uint64_t width;
    uint64_t height;
    struct pixel *data;
};

struct image some_image(const uint64_t width, const uint64_t height, struct pixel *data);
struct image image_create(const uint64_t image_width, const uint64_t image_height);
void image_destroy(const struct image* image);

#endif //ASSIGNMENT_IMAGE_ROTATION_IMAGE_H
