#include <stdio.h>

#include "image/image.h"

#include "util/bmp.h"

#include "util/io.h"

#include "transformations/rotate.h"


#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>

#define ERROR_CODE 1

static void log(const char *message);

static void log_stdin(const char *message);

static void clean_up(struct image image, FILE *file);

int main(int argc, char *argv[]) {
    if (argc != 3) {
        log("Check the number of args");
        return ERROR_CODE;
    }
    FILE *input = NULL, *output = NULL;
    struct image image = {0};
    char *input_filename = argv[1], *output_filename = argv[2];

    enum io_return_code code = file_open(input_filename, &input, "r");
    log(io_return_code_string[code]);
    if (code != OPEN_SUCCESS)
        return ERROR_CODE;

    enum read_status read_status = from_bmp(input, &image);
    log(read_status_message[read_status]);
    if (read_status != READ_OK)
        return ERROR_CODE;

    struct image transformed_image = transform(image);
    clean_up(image, input);
    if (transformed_image.data != 0) {
        log("Image is transformed");
    } else {
        log("Image was not transformed");
        return ERROR_CODE;
    }

    code = file_open(output_filename, &output, "w");
    log(io_return_code_string[code]);
    if (code != OPEN_SUCCESS)
        return ERROR_CODE;

    enum write_status write_status = to_bmp(output, &transformed_image);
    log(write_status_message[write_status]);
    if (write_status != WRITE_OK)
        return ERROR_CODE;
    clean_up(transformed_image, output);
    log_stdin("End with return code 0");
    return 0;
}

static void log(const char *message) {
    fprintf(stderr, "%s\n", message);
}

static void log_stdin(const char *message) {
    printf("%s\n", message);
}

static void clean_up(struct image image, FILE *file) {
    image_destroy(&image);
    file_close(file);
}
