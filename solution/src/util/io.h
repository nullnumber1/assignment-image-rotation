//
// Created by Artemij on 25.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_IO_H
#define ASSIGNMENT_IMAGE_ROTATION_IO_H

#include <stdbool.h>
#include <stdio.h>

enum io_return_code {
    OPEN_SUCCESS = 0,
    CLOSE_SUCCESS,
    OPEN_FAILED,
    CLOSE_FAILED,
    FILE_CLOSED,
};

extern const char *io_return_code_string[];

enum io_return_code file_open(const char *file_name, FILE **file, const char *mode);

enum io_return_code file_close(FILE *file);

#endif //ASSIGNMENT_IMAGE_ROTATION_IO_H
