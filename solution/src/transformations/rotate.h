//
// Created by Artemij on 27.12.2021.
//

#ifndef ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
#define ASSIGNMENT_IMAGE_ROTATION_ROTATE_H

#include "../image/image.h"

struct image transform(struct image source);

#endif //ASSIGNMENT_IMAGE_ROTATION_ROTATE_H
