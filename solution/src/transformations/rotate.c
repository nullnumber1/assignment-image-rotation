//
// Created by Artemij on 27.12.2021.
//

#include "rotate.h"
#include <mm_malloc.h>

static uint64_t pixel_index_output(size_t i, size_t j, struct image source) {
    return source.height * j + (source.height - 1 - i);
}

static uint64_t pixel_index_input(size_t i, size_t j,struct image source) {
    return i * source.width + j;
}

static struct image rotate_left(const struct image source, struct pixel* pixels) {
    uint64_t input_index;
    uint64_t output_index;
    for (size_t i = 0; i < source.height; ++i) {
        for (size_t j = 0; j < source.width; ++j) {
            input_index = pixel_index_input(i, j, source);
            output_index = pixel_index_output(i, j, source);
            pixels[output_index] = source.data[input_index];
        }
    }
    return some_image(source.height, source.width, pixels);
}

struct image transform(const struct image source) {
    if (source.data == NULL)
        return some_image(source.width, source.height, NULL);

    struct pixel *pixels = malloc(sizeof(struct pixel) * source.width * source.height);
    return rotate_left(source, pixels);
}
